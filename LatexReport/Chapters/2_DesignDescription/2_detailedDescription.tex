\newacronym{lab}{LAB}{Logical Architecture Blank}
\newacronym{lcbd}{LCBD}{Logical Component Breakdown Diagram}
\newacronym{ipfs}{IPFS}{InterPlanetary File System}

\clearpage
\section{Logical Architecture}
\label{sec:detailedDescription}

This section will focus on a more detailed and technical description of the
design of the system. As mentioned in \cref{sec:logicalAnalysis}, we will focus
on the different logical functions and the definition of subsystems. We define
\textbf{how} the system can fulfill our expectations \autocite{logicalArchiTuto}.

As in \cref{sec:overview}, this section will first focus on the robotic
architecture, then the {\DH}App architecture and lastly, the full logical
architecture.

% ############################################################################ %
\subsection{Robotic Architecture}
\label{sec:roboticArchitecture}

\subsubsection{Interesting \gls{ros} Packages}
\label{sec:detailsROS}

\paragraph{ROS and multirobots}

\gls{ros} uses a central node, named \verb+roscore+, to handle the communication
between the other nodes. \gls{ros} has been designed to be implemented on a
single robot. Because the master node supports port switching, it is possible
to use different nodes running on different devices but linked to the same
\verb+roscore+ node \autocite{roscoreWiki}. However, every robot's internal
communication will be processed by this master node.

To be able to have a more balanced load, there exist synchronization packages,
such as \verb+multimaster_fkie+ \autocite{multimaster_fkieWiki}, which aim at
establishing and managing a multi-master network. Hence, each robot has its own
master node and is able to interact with other robots. However, induced traffic
overhead is not documented. According to
\textcite{multimaster_fkieTechnicalReport}, the framework presents good
performance on a wired connection but care should be taken concerning power
issues and interferences when using a wireless interface such as Wi-Fi.

Even though these options are slightly out of the scope of this project, it is
interesting to consider because it brings modularity and flexibility to a
\gls{mrs}. By carrying everything they need, robots are more "ready to be
deployed" than with a single \verb+master+ node. We discuss this in
\cref{sec:deploymentOptions} and in \cref{sec:futureWork}. In addition, they do
their own logging without overloading the single \verb+master+ node. To make
sure, robots do not tamper their logs, they would save them in a secure storage
such as \gls{ipfs}. See \cref{sec:ipfsApp} for more information on this.

\paragraph{Navigation Stack}

ROS Navigation Stack is, in a summary, a differential driving controller
including a path planner and a set of recovery behaviours
\autocite{navStackWiki}. It is quite simple to operate with: a goal position is
sent to the stack which plans a path to reach the location and drives the robot
there while sending feedback messages to monitor progress. In terms of proximity
with the hardware, this is the closest package.

One of the requirements discussed in \cref{sec:designRequirements} is to have
a modular implementation of the robotic system. Using the Navigation Stack
fits in this, as it connected to the open-market by a simple interface that
is discussed in the next paragraph.

\subsubsection{Global View of the Architecture}

\Cref{fig:lcbd} shows the \gls{lcbd}. Compared to the previous breakdown
diagram (see \cref{fig:simplifiedROS}), the node \verb+traderNode+ has been
split into two components: an \verb+auctioneer+ and a \verb+participant+ and a
node named \verb+decisionNode+ has been created. Each robot runs its own
instance of these different nodes.

\begin{figure}[!h]
    \centering
    \includegraphics[width=\textwidth]{Capella/LCBD_Logical_System_ROS_LA.png}
    \caption{LCBD - Logical Component Breakdown Diagram}
    \label{fig:lcbd}
\end{figure}

\subsubsection{Explanations}

\paragraph{bc\_handler}

The only purpose of this node is to be an uplink interface between the robot
and a {\DH}App. Its only two functions are 

\begin{itemize}[noitemsep]
    \item 
        sending the result of auctions in a message including the \gls{pk} of
        the seller, of the buyer and the task id.
    \item
        signaling to the {\DH}App that a task has been done by sending its id
        and the \gls{pk} of the robot.
\end{itemize}

This choice has been motivated by the will of keeping one job per node.
Hence, \verb+traderNode+ is only in charge of dealing with auctions. 
Another important aspect is, if the {\DH}App is unresponsive at some point, 
\verb+bc_handler+ will try to contact it a few seconds later.

\paragraph{traderNode \& decisionNode}
Splitting up \verb+traderNode+ into two subcomponents is quite obvious because
this node also participates in auctions another instance triggered. However,
\verb+decisionNode+ needs further explanations: this node is in charge of
storing tasks that have been won by the trader in memory (a \verb+vector+ object
is used) and selecting one task among those available. Then, it checks, with
the \verb+metricsNode+ if the task is not too expensive to achieve. Indeed,
the situation of the robot could have changed between the moment the task
has been won and the moment the task has been selected to be performed. If
the benefit is too low, the task is sent back to the trader to be sold; 
otherwise, it is sent to the \verb+execNode+.

\textit{ \textbf{Note:} for simplification purposes, at the moment, we
    authorize only one auction at a time and if the robot is idle, meaning the
    robot is not busy with a mission. Consequently,} \verb+decisionNode+
    \textit{has, at most, one task stored in memory.}

\paragraph{execNode}

This node is in charge of decomposing a task into two elements: a goal position
to go to and a task to achieve. It first sends the goal position to the
Navigation Stack and listens to its feedback. Once the position has been
reached, the action associated with the task is performed. Then, the node
signals the \verb+bc_handler+ that the task has been done successfully.

If a problem happens such as, the robot is unable to achieve its goal or if a
remote deactivation has been triggered (more on this in
\cref{sec:autoExclusion}), the task is directly sent to the \verb+traderNode+.
That way, another robot of the team could do it and the task would not be lost.

Lastly, this node broadcasts the status of the robot (\verb+idle+ or not) so
that other nodes can wait for the robot to be free if necessary.

\paragraph{Conclusion}

\Cref{fig:ROS_Architecture} and \cref{table:connectionNodes} summarize how the
different nodes are connected to each other. The third column shows from which
node and the data type that are received; while the fourth column shows the
type of data sent to others.

\begin{figure}[!h]
    \centering
    \includegraphics[width=\textwidth]{ROS_Architecture.png}
    \caption{ROS Architecture highlighting the Different Nodes Interactions}
    \label{fig:ROS_Architecture}
\end{figure}

\subsubsection{Auction Proceedings}

\Cref{fig:es_auctionDApp} is a detailed sequence diagram of an auction,
starting from the message sent by the {\DH}App and received by the auctioner.
In this case, the components \verb+bc_handler+ and \verb+auctioneer+ are
running on the \gls{ros} server while the other nodes are running on a robot.

The double-sided arrows on the \verb+auctioneer+ section mean that the auction
has a finite time of 5 seconds. This value has been arbitrarily chosen and is
required because we cannot know how many robots will bid in advance. This is
hence a necessary delay overhead.

\begin{sidewaysfigure}
    \centering
    % \vspace*{0.5cm}
    % \advance\leftskip-2.8cm
    \includegraphics[width=25cm]{Capella/ES_Scenario_Auction_From_DApp.png}
    \caption{Sequence Diagram of an auction triggered from the {\DH}App.}
    \label{fig:es_auctionDApp}
\end{sidewaysfigure}

% ############################################################################ %
\clearpage
\subsection{{\DH}App Architecture}
\label{sec:dappArchitecture}

\subsubsection{A Private Ethereum Network}

As stated in \cref{sec:dappDesign}, we instantiated a private Ethereum chain by
creating and using a different genesis block from the main network. Each agent
is manually added in order to control who is allowed to access the network. We
discussed this idea in \cref{sec:clusteringNetwork}, which enables us to
decrease or remove the \gls{pow}. In the context of this project, we have two
miners running on the same computer\footnote{{geth}, the client we use, seems
    to have a problem with propagating transactions on a private network with a
    single miner. Using two miners solves this issue
    \autocite{issueGethMiner}}.

\subsubsection{Interacting with the User}

An operator can interact with the system by using the {\DH}App. It is a web
interface containing a form request describing a new mission to be deployed on
the network. This interface also displays information on the different tokens
available, which wallet owns them (and hence, which robot), information on the
mission and if the task has been done already or not.

\subsubsection{Interacting with ROS and the Blockchain}

\paragraph{Communication with ROS}

From the \gls{ros} point of view, the server is another robot which takes part
in the auction process, only as auctioneer, never as a participant. This makes
the design of the system simpler as we use the same nodes as the robots. As
this 'robot' is static, it only uses the \verb+bc_handler+, \verb+traderNode+
and a slightly modified \verb+metricsNode+ which always sends back an infinite
cost.

The {\DH}App uses \verb+roslibjs+, a JavaScript library \autocite{roslibjsWiki}
which aims at providing a JavaScript interface with \gls{ros} through
WebSockets. This requires to have a server running on the \gls{ros} network
called \verb+rosbridge+.  To auction a new task, the {\DH}App contacts the
server's \verb+traderNode+ running in the same namespace as the
\verb+rosbridge+ node.

To get the result of an auction or the signal that a task has been done, the
{\DH}App advertises two distinct services that a \verb+bc_handler+ node
(running on the server or on another robot) can contact.

\paragraph{Communication with a blockchain node}

The main feature of the {\DH}App is to interact with a blockchain node in order
to query information and to send transactions to be mined.
To do so, we use \verb+web3js+, a JavaScript library which implements JSON
RPC specifications (a communication protocol over HTTP) \autocite{web3jsWiki}.

This library lets us query the blockchain to get the list of wallets and the
list of tokens already deployed. Once the information about these has been
received, they are displayed on the web interface. \verb+web3js+ also lets us
send transaction such as the creation of token or the transfer of ownership
of an already existing token.

\subsubsection{Sequence Diagram}

\Cref{fig:es_creationTaskDApp} shows the sequence diagram of the different
interactions that happen between the user, the blockchain and, the \gls{mrs}.


\begin{figure}[!h]
    \centering
    \vspace*{0.5cm}
    \advance\leftskip-1.6cm
    \includegraphics[width=18cm]{Capella/ES_Scenario_Mission_Creation_DApp.png}
    \caption{Sequence Diagram of an auction triggered by the {\DH}App.}
    \label{fig:es_creationTaskDApp}
\end{figure}

The deployment of a token happens as follow:
\begin{itemize}
    \item 
        The operator inputs a new mission.
    \item 
        The {\DH}App triggers a transaction \verb+mint+ to create a new token.
        The wallet which triggered the transaction (ie. the server's) owns it.
        We get the token id in the return value.
    \item 
        The {\DH}App creates and ROS message and sends it to the
        \verb+traderNode+ instance running on the server. It will open an
        auction among the other robots.
    \item
        At the end of the auction, the {\DH}App receives a message with the
        seller's \gls{pk}, the buyer's and the token's id. It then triggers
        a \verb+transferOwnership+ transaction. In the meantime, the task
        is performed.
    \item
        Once the task is done, the {\DH}App receives a message from the
        \gls{ros} system. It then triggers a \verb+setTaskDone+ transaction.
\end{itemize}

The two last steps also happen if a robot triggers an auction (if the cost
of a task became too expensive for instance).

\textbf{Note:} Calls to a blockchain can be time-consuming, especially when a
transaction is concerned. Hence every call is asynchronous to avoid the
interface to be unresponsive. On \Cref{fig:es_creationTaskDApp}, the three
functions of the blockchain have undermined duration. Lastly, the web interface
is refreshed after the execution of every callback function to keep the
operator updated on the state of the system.


% ############################################################################ %
\subsection{System Architecture}
\label{sec:systemArchitecture}

In this section, we will combine the robotic architecture discussed in
\cref{sec:roboticArchitecture} and the {\DH}App architecture discussed in
\cref{sec:dappArchitecture}. To do so, we will have a look at the \gls{lab}
Diagram (\cref{fig:lab_deployChain}).

According to Capella's description, the \gls{lab} aims at \textit{"Allocat[ing]
    logical functions to logical components"}. The green boxes are \textit{Logical
Functions} which are linked by \textit{Functional Exchanges}.

\begin{sidewaysfigure}[!h]
    \centering
    % \vspace*{0.5cm}
    % \advance\leftskip-1.6cm
    \includegraphics[width=24cm]{Capella/LAB_Logical_System_MissionDeploymentDApp.png}
    \caption{LAB - Logical Architecture Blank Diagram}
    \label{fig:lab_deployChain}
\end{sidewaysfigure}

As on \Cref{fig:sab_deployChain}, the blue line is a Functional Chain. To
highlight the difference between the \gls{sab} (\cref{fig:sab_deployChain}) and
the \gls{lab} (\cref{fig:lab_deployChain}), the same Functional Chain is shown.
The path shows the combination of the two sequence diagrams we studied earlier
(\cref{fig:es_auctionDApp} and \cref{fig:es_creationTaskDApp}) with the
logical components shown in the \gls{lcbd} (\cref{fig:lcbd}).

More \gls{lab} figures can be found in \cref{sec:labApp}:
\Cref{fig:lab_deployChainRobotPOV} shows the \gls{lab} with the Functional
Chain highlighting a mission deployment from the robots point of view and
\Cref{fig:lab_missionReauction} shows the Functional Chain of a task re-auction
if it became too expensive to perform.

% ############################################################################ %
\subsection{Summary}

This concludes the section on the Logical Architecture of the system. In the
next section, we will have a look at different deployment options.
This final stage of a top-down approach of the System Engineering concerns
the Physical Architecture of the project. However, this project is about a
software implementation. In this context, studying deployment options fits
this final step.
