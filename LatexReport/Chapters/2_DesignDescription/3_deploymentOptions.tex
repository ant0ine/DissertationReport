\clearpage
\section{Different Deployment Options}
\label{sec:deploymentOptions}

In this section on the deployment of this project, we will discuss four
different options which are more or less centralized. As this dissertation is
about studying the added value of using a blockchain to solve issues of
accountability, reliability and trust, the ideal scenario would be to
investigate and implement these different options and, then compare them.

We have three different parameters that influence the level of distribution or
centralization of the system. These are the followings:
\begin{itemize}
    \item
        Having a single \verb+rosmaster+ or using a package such as 
        \verb+multimaster_fkie+ as discussed in \cref{sec:roboticArchitecture}.
    \item
        Having a single instance of the {\DH}App running on the server and 
        contact it from the server's \gls{ros} instance (see \cref{fig:serverRobotSingleDApp}) or
        having an instance and a blockchain (non-mining) node on each robot
        (see \cref{fig:distributedDApp}) with their own local {\DH}App.
    \item
        Having the auction implemented on \gls{ros} or using a smart-contract
        handling the bids and selecting the winner.
\end{itemize}

We could also have a system only composed of the robotic architecture
(\cref{sec:roboticArchitecture}) without any blockchain. This would be used
as a reference for overhead measurements. \Cref{fig:deploymentOptionsTree}
shows the different options we have with these different parameters.

\begin{figure}[!h]
    \centering
    \includegraphics[width=\textwidth]{DeploymentOptionsTree.png}
    \caption{Tree corresponding to the Different Deployment Options}
    \label{fig:deploymentOptionsTree}
\end{figure}

% ############################################################################ %
\subsection{Option 9 - No Blockchain - Single ROS master}

\begin{figure}[!h]
    \centering
    \includegraphics[width=\textwidth]{SingleROS_NoDApp_FeedbackMauro.png}
    \caption{MRS System without Blockchain Implementation}
    \label{fig:SingleROS_NoDApp}
\end{figure}

Figure \ref{fig:SingleROS_NoDApp} shows an early development stage of 
this project. This corresponds to a fully working open-market economy for
\gls{mrs} but without the implementation of a blockchain to record the
missions and the transactions.

% ############################################################################ %
\subsection{Option 1 - Single ROS master - Single {\DH}App - Auction on ROS}
\label{sec:option1}

\subsubsection{Presentation}

\begin{figure}[!h]
    \centering
    \includegraphics[width=.8\textwidth]{ServerRobots_SingleDApp_FeedbackMauro.png}
    \caption{Server Mining Block, Robots are Clients through the {\DH}App}
    \label{fig:serverRobotSingleDApp}
\end{figure}

Figure \ref{fig:serverRobotSingleDApp} is slightly more complex as the {\DH}App
is implemented. In this case, the robots all interact with the blockchain
through the same {\DH}App, which is also used by the operator to deploy
missions on the \gls{mrs}.

As discussed in \cref{sec:roboticArchitecture}, here the \textit{ROS Server}
acts like a static robot, running the \verb+rosbridge+ node to interact with the
{\DH}App and being the \verb+rosmaster+.

From a networking point of view, the server, the miners, and the {\DH}App can
be on three (or more) different workstations. Indeed the {\DH}App is simply
connected to two different \verb+rpc+ servers, one being a blockchain node,
the other one, a \verb+rosbridge+ node.

\subsubsection{Implementation}

This architecture is the first to be implemented. In our simulation, two
miners, the {\DH}App, the ROS Server and different Turtlebots were on the same
workstation.

% ############################################################################ %
\subsection{Option 3 - Single ROS master - Distributed {\DH}App - Auction on ROS}
\label{sec:distributedDApp}

\begin{figure}[!h]
    \centering
    \includegraphics[width=.9\textwidth]{DistributedDApp.png}
    \caption[Architecture with Distributed {\DH}Apps]
        {Each Robot runs a non-mining node and carries its own {\DH}App to interact with it.}
    \label{fig:distributedDApp}
\end{figure}

Figure \ref{fig:distributedDApp} shows a more distributed deployment of the
system. Compared to \cref{sec:option1}, each robot has a local blockchain
node. These are only clients and do not participate in the mining process
because robots do not have enough processing power to do it. Note that these
are full nodes, and are as part of the network than any other node.

Because robots have their own local node, this requires the use of an interface
to interact with it, hence the presence of the {\DH}App. Because it has been
implemented in JavaScript, an engine such as V8 (used by NodeJS
\autocite{nodeJSVM}) runs on the robots as well. In this scenario, the robots
are still connected to the same \verb+roscore+ node which runs on the \gls{ros}
Server.

% ############################################################################ %
\subsection{Option 2 or 4 | 6 or 8 - Auction on Smart-Contract}

This option is fundamentally different from the previous ones.  Instead of
having the open-market economy fully implemented in a \gls{ros} system with a
blockchain layer on top of it, this option brings the auction mechanism within
a smart-contract on the blockchain.

This deployment option is a completely different design on the auction
mechanism.  Instead of having a \verb+traderNode+ handling it, the
\verb+bc_handler+ would make the different calls. Moreover, as the auctions
will be fully distributed, as running on the blockchain, a distributed
implementation of the {\DH}App makes more sense here in order to avoid a heavy
load on the main server, and avoid, hence, a single point of failure. Moreover,
tampering such auction would be harder as the processing would be done through
the blockchain.

This option has not been spotted later in the development of this project as
you had decided to focus the addition of a blockchain on top of an open-market
economy for \gls{mrs}. We discuss this more in \cref{sec:evaluation}.
