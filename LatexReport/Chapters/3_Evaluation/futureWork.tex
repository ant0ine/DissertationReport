\clearpage
\section{Future Work}
\label{sec:futureWork}

This section will discuss different ideas to improve the system at different
scopes: short term, medium term or long term. These can be seen as objectives
to reach for the three next versions of the system.

% ############################################################################ %
\subsection{Short Term}
\label{sec:shortTerm}

This section will focus on improvements that are the most urgent or the most
interesting to implement as soon as possible.

\paragraph{Software Testings} such as stress tests or integration tests are
required in order to make sure the system is stable and does not crash. Even
though, this kind of testing has already been done through the different
developing stages, it should be done after the implementation of every major
features or change in already implemented ones.

Moreover, security is a major topic in smart-contracts development and unit
tests are extremely important because a mined blockchain transaction usually
involves money and is immutable. Unfortunately, my knowledge in security, in
Solidity (the programming language to develop Ethereum smart-contracts) and
security testing are limited and I have not allocated a lot of resources on
this topic.

This testing is named "Continuous Integration" and is a major part of the Agile
methodology (\cref{sec:agile}).

\paragraph{Packaging the {\DH}App} into a node program in order to use it in a
JavaScript engine different from a Web browser is planned. This would enable
the {\DH}App to be distributed on every robot, as seen in
\cref{sec:distributedDApp}. Each distributed {\DH}Apps would be connected to a
local non-mining \verb+geth+ client.

\paragraph{Tests on Robots} is the next step. There are Turtlebots robots
available at Heriot-Watt University which could be used to host the
open-market.  Testing on real robots would also decrease the load on the
\gls{ros} server.

\paragraph{Improving Transactions Safety} by solving the transaction signature
issue. To trigger a transaction from a wallet, either its password must be
provided, or the wallet must be unlocked. In this case, there is no security
left, as anybody can use the account to send transactions from it. Once again,
this reaches the limit of my knowledge in cryptography and software security.
This is a weakness discussed in \cref{sec:systemWeaknesses}.

\paragraph{Improving Tasks Deployments} is a low priority task but quick to
implement: at the moment, if tasks are deployed too fast in the system,
they do not have time to be properly auctioned to the fittest robot. A quick
solution is to send tasks to the \verb+decisionNode+ in the \gls{ros} Server
and not directly to the \verb+traderNode+. This would let the server to store
tasks, and as the custom \verb+metricsNodePC+ will block tasks to be sent to a
non-existing \verb+execNode+, they will be auctioned one after the other.
This does not require a lot of work.

\paragraph{Allow Multiple Tasks in Memory} would give more sense to the
"re-auction" feature. However, this would need more changes in the code than
the previous idea because a new topic is required not to consider a "dead"
robot as busy as it is at the moment.

% ############################################################################ %
\subsection{Medium Term}
\label{sec:mediumTerm}

\paragraph{ROS \& Multimaster Packages} Once the previous ideas have been
implemented, we could deploy the \verb+multimaster_fkie+ package on the robots.
As discussed in \cref{sec:detailsROS}, this would enable a more clustered
\gls{ros} network. There may be some traffic overhead due to the
synchronization, but it would be balanced by individual logging and a solution
more ready to be used.

\paragraph{Implementing a Basic Task Taxonomy} in order to have different tasks
to do and to have a heterogeneous team of robots. As the system is modular
by design, only the \verb+metricsNode+ and \verb+execNode+ need to be adapted,
as long as the \verb+action+ message included in a \verb+task+ message.

\paragraph{More Testing} is required with such heavy new features, in
simulation, and with robots. The previous idea could be done either with a
single {\DH}App design or the distributed variant as it is independent of the
blockchain integration in the \gls{mrs}.

\paragraph{Failure in Achieving a Task} is actually handled by a counter in
the \verb+task+ message. Every time an instance of the \verb+decisionNode+
receives a task, this counter is incremented and then checked. If it is higher
than five, then the task is discarded and
only a \verb+ROS ERROR+ message is raised. It would be interesting to record
this on the blockchain. To do so, an update of the token is required as long
as a function to signal it. Then the interface would display the status of the
discarded task.

\paragraph{Updating the Token Architecture} to a safer implementation than
a JSON string as it was originally planned. Because I struggled to interact
with the smart-contract by using the JavaScript API, I simplified the token
to the maximum. 

\paragraph{Transferring Rewards} When a robot performs a task, a transaction
could be addressed to its account as a reward for having performed it.
Implementing the corollary is also advised (the costs spent by the robots do
the task and the bid made to win it). However, this requires a way to measure
the distance traveled by a robot and the time elapsed since the start of the
task. Transferring the reward could be done in Ether or by transferring a custom
ERC20 token we would deploy on our network (see \cref{sec:ercTokens}).

\paragraph{Refactoring the Code} to make it more readable, optimize it and
remove useless instructions. Having less code and better quality code help
to decrease maintenance and new features implementation costs.

% ############################################################################ %
\subsection{Long Term}
\label{sec:longTerm}

\paragraph{Tasks Synchronization at Startup} At the moment, when the system is
launched, robots wait for a new mission to be inserted by the operator. It
could be interesting to have a synchronization at boot which would consist in
crawling the blockchain tokens to download tasks that have not been done yet.
To do so, having a local {\DH}App and a local node on each robot would be
easier and more stable than having a central {\DH}App.

\paragraph{Improve the Taxonomy} in order to improve the system and add
disturbance considerations: two robots could need access to the same resources
for two different tasks. They would have to negotiate and compromise.

A basic example of this is, a robot has to stay in a small corridor (let's say
it is painting the wall), then a second robot has to cross this corridor, but
to do so, the first robot has to move to let it go through. In this scenario,
the two robots have to reach an agreement, such as the second robot pays a fine
to compensate the first robot for its breach of contract. Otherwise, the second
robot would have to wait for the first robot to be done.

\paragraph{Tight Coordination Between Robots} is an extension of the previous
paragraph. It would consist in having missions requiring at least two robots
working together to achieve their goal. An example is the box pushing mission
introduced in \autocite{AuctionMethodsForMultirobotCoordination}.

\paragraph{Recursive Auctions} requires to deeply redefine the design of the
auction mechanism. However, this would bring robustness and capabilities to the
\gls{mrs} as it would be able to perform more complex tasks. This idea is an
improvement of the proposition on tight coordination and a more detailed
taxonomy. It consists in having robots analyzing deeper the task before bidding
on it with the help of a planner. This planner would be able to generate
"sub-tasks" that would be "sub-auctioned" before the robot bids on the main
mission.

Let's imagine we have a digger robot and a grabber. The mission is to extract an
object buried in the ground. Before bidding on the mission, the grabber would
have to contact the digger to be sure it would be able to perform the tasks
together.

\paragraph{\gls{ipfs}} is a protocol aiming at storing and sharing information
in a distributed file system (more information in \cref{sec:ipfsApp}). Briefly,
this lets agents store data that are too heavy to be recorded on a blockchain.

Using this file system in the context of this project would enable robots to
safely and securely store their mission logs and data they would create. Then,
they could sell the access to this data. For instance, a thermal
sensor-equipped robot just spent a week doing measurements in a remote area. As
this location was difficult to reach, it could not do anything else. Because of
this, other robots or users could be interested in getting the records by
buying an access to it. Moreover, by sending the hash of a set of logs in a
transaction, we could later prove that data are genuine.

\paragraph{Use hyperledger\_fabric} as a blockchain basis for the whole system.
We already mentioned it in \cref{sec:dappDesign} (more information are
available in \cref{sec:hyperledger_fabricApp}). This project enables users to
use a permissioned blockchain and easily use different chains in parallel while
being able to deploy and use smart-contracts. The point of implementing this
relies on the ability of cluster large \gls{mrs} into smaller groups. As
discussed in \cref{sec:clusteringNetwork}, we would have different blockchains,
each one of them having a specific purpose.  This leads us to the next
paragraph.

\paragraph{Test with Another Cluster} and build a network with every idea
discussed above would highlight the feasibility (or not) of an industrial
deployment of this project.

\paragraph{Detect a Bad Robot} and analyze the added-value of blockchain
in this case. This could be done by having a marking system of robots: the
seller would mark the quality of the job done by the buyer and the buyer could
mark the sellers. Having a two-way system would enable a fairer notation.
Then, robots could decide to work or not with a robot, and choose one with
a better reliability but more expensive, based on this metrics.

