\section{Testing of the Requirements}

In this section, we evaluate qualitatively the implementation of the features of
the system and its respect to the requirements discussed in
\cref{sec:designRequirements}. All the testings have been made in simulation
using \gls{ros} development tools such as Rviz \autocite{rvizWiki} and Gazebo
\autocite{gazeboMainPage}.

The workstation on which tests have been conducted is a HP Z240 with an Intel
i7-7700 CPU, a NVidia Quadro P600 graphics card and 16Go of DDR4 RAM. It runs
Ubuntu~16.04 with \gls{ros} Kinetic installed.

\subsection{Software Testing Methodology}

Testing a software solution is usually divided into four different steps:
\begin{itemize}[noitemsep]
    \item 
        \textbf{unit testing} which corresponds to the testing of individual
        modules such as functions;
    \item
        \textbf{integration testing} which corresponds to the testing of
        different modules together;
    \item
        \textbf{system testing} the phase where the entire system is tested;
    \item
        \textbf{acceptance testing} in which we check that the design
        requirements have been met.
\end{itemize}

On top of that, it is common to also test the performances of the system,
its security, its usability by the end user and its compatibility. These are
non-functional testings \autocite{softwareTestingMethods}.

% ############################################################################ %
\subsection{Evaluation of the System}
\label{sec:systemEval}

We will only focus on the system, acceptance testings as well as a
non-functional testing analysis.

\subsubsection{Using \& Testing the System}

To use the system, first the two miners are started; then the smart-contract is
deployed again on the blockchain. If it has already been deployed, a
re-deployment has the same effect as a new deployment by using the
\verb+--reset+ option.

Then \verb+lite-server+, a node module \autocite{lite-serverNPM} aiming at
serving a web app for development purposes, is started. This server lets us use
browser add-ons such as Metamask \autocite{metamaskMainPage} to interact with
our local {\DH}App. This add-on is able to sign transactions before sending
them to a node to be mined. However, this caused a validation pop-up to appear
to confirm every transaction made by the \gls{mrs}. Its use has been, hence,
postponed and signing transactions is discussed in \cref{sec:futureWork}.

Finally, the \gls{ros} simulation is launched: first Gazebo, rviz and, the
robots are instantiated, then our \gls{ros} nodes. These have to be launched
afterward for two reasons: 

\begin{itemize}
    \item 
        We make sure everything is running, including an instance of the
        Navigation Stack of every robot, before starting our nodes. Sometimes,
        \gls{ros} will start our nodes before the Navigation Stack which
        prevents a proper connection between them because the server to which
        goal positions are sent may not have started yet.
    \item
        During the development stage, being able to only restart our nodes
        without restarting the whole simulation is time-saving. In the case of
        a demonstration or a testing, it can be used as a reset to the initial
        conditions.
\end{itemize}

\subsubsection{The Metrics Function}

The metrics function, giving an estimation of the cost of performing a task, is
quite basic as seen in \cref{lst:metricsNode}: it first computes the cartesian
distance between the current position and the target given in the \verb+Task+
message (see \cref{lst:calcMovingCost}). This result is multiplied by $10$ to
give it more strength because, in our simulations, distances are very small.
Moreover, moving is consuming more energy than doing measurements. Lastly, the
value \verb+waitTime+ is added to the cost.

\begin{lstlisting}[language=C++,
                    frame=single,
                    basicstyle=\footnotesize,
                    caption={Cost of Performing a Task},
                    label={lst:metricsNode}]
metrics.cost = 10*calcMovingCost(myPose, Task.task.goalPosition_p)
             + Task.task.toDo.waitTime;
\end{lstlisting}


\begin{lstlisting}[language=C++,
                    frame=single,
                    basicstyle=\footnotesize,
                    caption={Function to Compute the Distance between two Points},
                    label={lst:calcMovingCost}]
float calcMovingCost(geometry_msgs::Pose curPose,
                     geometry_msgs::Pose targetPose  )
{
    float result = pow(curPose.position.x - targetPose.position.x, 2)
                 + pow(curPose.position.y - targetPose.position.y, 2) 
                 + pow(curPose.position.z - targetPose.position.z, 2);
    return sqrt(result);
}
\end{lstlisting}

This cost value does not take into account the path the robot will actually do
to go to the position. Another could be slightly farther and lose the auction
whereas the winner has an obstacle to avoid, which is not taken into
consideration here. We discuss this in \nameref{sec:futureWork}.

\subsubsection{Functional Chain - Task Deployment}

\paragraph{Input a New Task}

First, the user inputs a new task by fulfilling the form on the web page
(\cref{fig:1_inputTask}). At first, there is no token deployed on the blockchain
as shown in \Cref{fig:2_emptyAccount}.

\begin{figure}[!h]
    \centering
    \begin{minipage}{0.48\textwidth}
        \centering
        \includegraphics[width=.9\textwidth]{EvaluationTaskDeployment/1_inputTask.png}
        \caption{Form to Input a New Task to Do.}
        \label{fig:1_inputTask}
    \end{minipage}
    \begin{minipage}{0.48\textwidth}
        \centering
        \includegraphics[width=.9\textwidth]{EvaluationTaskDeployment/2_accountEmpty.png}
        \caption[List of Accounts on The Network.]
                {List of Accounts on The Network. The first one is the server's account.}
        \label{fig:2_emptyAccount}
    \end{minipage}
\end{figure}

\Cref{fig:0_rviz,fig:0_gazebo} show the simulation initial state.

\begin{figure}[!h]
    \centering
    \begin{minipage}{0.50\textwidth}
        \centering
        \includegraphics[width=.9\textwidth]{EvaluationTaskDeployment/0_gazebo.png}
        \caption{Gazebo at System Startup}
        \label{fig:0_gazebo}
    \end{minipage}\hfill
    \begin{minipage}{0.50\textwidth}
        \centering
        \includegraphics[width=.9\textwidth]{EvaluationTaskDeployment/0_rviz.png}
        \caption{Rviz at System Startup}
        \label{fig:0_rviz}
    \end{minipage}
\end{figure}


\paragraph{Deployment of Two Tokens}

Let's create two tasks. First, we want a robot to go to the point
\verb+(3; 0.5; 0)+ and wait there for \verb+5+ seconds. Then, we want a robot
at the location \verb+(-1; -1; 0)+ and wait there for \verb+10+ seconds. Both
tasks are worth $100$ Eth.

\paragraph{Display Update}

After each transaction on the blockchain, the {\DH}App updates the display of
the different information. In \Cref{fig:3_accounts2Tokens}, the wallets of
robot \verb+id:1+ and \verb+id:2+ both own a token. These are visible in
\cref{fig:4_infoTokens2Tokens} which displays the id of the task, its owner,
its status (\verb+done+ or not), as long as its description in a JSON format.

\begin{figure}[!h]
    \centering
    \begin{minipage}{0.80\textwidth}
        \centering
        \includegraphics[width=.95\textwidth]{EvaluationTaskDeployment/3_accounts2Tokens.png}
        \caption{Updated Accounts Information}
        \label{fig:3_accounts2Tokens}
    \end{minipage}\hfill
    \begin{minipage}{0.99\textwidth}
        \centering
        \includegraphics[width=.95\textwidth]{EvaluationTaskDeployment/4_infoTokens2Tokens.png}
        \caption{Updated Tokens Information}
        \label{fig:4_infoTokens2Tokens}
    \end{minipage}
\end{figure}

\paragraph{Result in Simulation}

\Cref{fig:5_gazebo,fig:5_rviz} show the simulation tools after the execution of
the two tasks. In \Cref{fig:5_rviz}, we can see the calculated trajectory the
robots followed to achieve their respective goals.

\begin{figure}[!h]
    \centering
    \begin{minipage}{0.50\textwidth}
        \centering
        \includegraphics[width=.9\textwidth]{EvaluationTaskDeployment/5_gazebo.png}
        \caption{Gazebo after the Execution of the Two Tasks}
        \label{fig:5_gazebo}
    \end{minipage}\hfill
    \begin{minipage}{0.50\textwidth}
        \centering
        \includegraphics[width=.9\textwidth]{EvaluationTaskDeployment/5_rviz.png}
        \caption{Rviz after the Execution of the Two Tasks}
        \label{fig:5_rviz}
    \end{minipage}
\end{figure}

More information, such as logs, can be found in \cref{sec:logsTaskDeploymentApp}.

\subsubsection{Functional Chain - Re-Auction of a Task}

\paragraph{Description}

Because the situation of a robot could change between the moment a task has
been won by the \verb+traderNode+ and the moment the \verb+decisionNode+
selects it to send it to the \verb+execNode+, the cost of performing the task
is checked again. If it is too expensive, it will be sent to the
\verb+traderNode+ to be re-auctioned. Once the previous chain has been tested,
testing this one is quite straightforward.

This event can also happen when the Navigation Stack cancels a goal because it
cannot be reached after a few tries. In this case, it is the \verb+execNode+
which sends the task to the \verb+traderNode+ to be auctioned.

\paragraph{Testing}

By using \gls{ros} development tools, a message containing a task can be sent
to the \verb+decisionNode+. This task, forged to be too expensive to be done
(by giving a null reward or by giving a location that is outside the map known
to the planner), will be stored by the node and then selected. As it is not
interesting to be done, an auction is opened by the \verb+traderNode+ to sell
the task.

\paragraph{Improvements}

During the testing phase of this feature, it has been noticed, as it is
possible that no robots can reach a given position, that the task is
indefinitely re-auctioned by the different robots. To prevent that, a counter
variable has been included in the \verb+Task+ message. This counter is
increased every time by the \verb+decisionNode+ just before the task is stored
in memory. If this task has already been auctioned five times\footnote{This is
    an arbitrary value.}, then the node discards the task (see
\cref{fig:limitedReauctions}).

\begin{figure}[!h]
    \centering
    \includegraphics[width=\textwidth]{system_testing/limitedReauctions.png}
    \caption{A Task cannot be Auctioned Indefinitely}
    \label{fig:limitedReauctions}
\end{figure}

% ############################################################################ %
\subsection{Evaluation of the Robustness of the System}
\label{sec:robustnessEvaluation}

Here, we will focus on the robustness features defined in
\cref{sec:robustnessFailure}.

\subsubsection{Unplanned Obstacle on Path}

This feature is directly implemented in \gls{ros} Navigation Stack (see
\cref{sec:detailsROS}) which computes a path to follow. If sensors detect an
obstacle, the \verb+path planner+ will start a recovery behaviour and try to
find another path to achieve the requested goal.

Simulation tools are not highly flexible on this. During the testing of the
system, it happened twice that a path has been found to go to the desired
position. However, because the computed path made the robot go "outside" the
known map (even though the path was free), a recovery behaviour to find a new
path has been triggered. There seem to be some misalignment issues between
Gazebo and Rviz, and as they work together in the simulation, it can cause
problems independent of this project.

Meeting another robot is also an unplanned obstacle. However, when this happens,
Gazebo and rviz become out of sync: because Gazebo, as a 3D engine, deals with
the collision. Robots are not displayed at the same position on rviz anymore,
on which robots can cross each other. \Cref{fig:rvizGazeboCollision} shows
this problem after it happened.

\begin{figure}[!h]
    \centering
    \includegraphics[width=\textwidth]{system_testing/collisionRobots.png}
    \caption{Rviz and Gazebo do not deal with Collisions Differently. Rviz on
        the left, Gazebo on the right.}
    \label{fig:rvizGazeboCollision}
\end{figure}

\subsubsection{Auto Exclusion of a Robot}
\label{sec:autoExclusion}

This feature can be manually triggered by calling a service server\footnote{a
    service in \gls{ros} provides a "request/reply" form of communication. A
    client can contact a server and then waits to get its answer. It is often
    presented as a "remote procedure call" \autocite{serviceDocROS}.} running on
the \verb+execNode+. Consequently, another node, monitoring the battery level
for instance, could send the same signal to exclude the robot from the system
and then send it back to its charging station. 

If the signal is triggered, the current goal being executed by the Navigation
Stack is canceled and sent back to the \verb+traderNode+ in order to be
auctioned to another robot.  If the \verb+traderNode+ does not accept the
incoming task after 5 tries, the task is lost. This has been implemented in
order to limit an overload of the system.

Then the \verb+execNode+ advertises the robot as being \verb+busy+. Thus, the
other nodes are blocked (\verb+traderNode+ will not participate in any auction
and \verb+decisionNode+ will not send any new goals to the \verb+execNode+).

\subsubsection{Failure than Recovery}

This has been implemented alongside the previous feature. If another call is
made to the service server, it will restore the status of the robot to
\verb+idle+ and it will back in the team and be able to participate in
auctions. To re-use the same example as above, once the battery level would be
high enough, the monitoring node would re-send the signal to make the robot
available again.


\subsubsection{Robot Death}

The only way to detect if a robot system completely crashes at runtime is
to manually monitor \gls{ros} logs. To automatically detect this,
implementing a watchdog pulse as in
\autocite{AuctionMethodsForMultirobotCoordination} is a feasible idea.

% ############################################################################ %
\subsection{Non Functional Testings}
\subsubsection{Performance and Stress Testings}

Different parameters have been modified in this testing phase, including the
complexity of the blockchain mining, the delay in between triggering the
auction of a new task, the number of robots in the system and the total number
of tasks.

\paragraph{Mining Complexity}

As seen in \cref{sec:blockchainLR}, mining a blockchain is computationally
difficult because of the \gls{pow}. The first testings clearly showed that
decreasing the complexity was necessary because mining a new block rapidly
increased to a duration between $10$ and $20$ seconds. Even by specifying a
complexity of zero in the configuration of the genesis block, the complexity of
mining a block kept increasing as the blockchain was getting longer. This comes
from the wish to have a constant growth rate of the blockchain based on the
time required to mine the previous block and the available resources for
mining: Ethereum uses an automatic adjustment of the mining complexity
\autocite{difficultyAdjEth}.

To prevent this from happening on our private network, \verb+geth+, the
Ethereum client, has been recompiled without this feature following these
instructions \autocite{zeroComplexityGeth}.
\textcite{swarmRobotsBlockchainDorigo} also applied this solution in order to
decrease the number of variables during the conducted experiments and because
of the lack of processing power of the simulated robots.

The difference in the mining process is clear: after a few hours of mining the
blockchain, adding a new block is almost instantaneous: between $0$ and $4$
seconds, with an average time of $2$ seconds. This was the parameter having the
strongest on the system because robots were waiting for transactions to be
processed most of the time.

Be aware that, by doing so, the local blockchain is not secured and requires
only trusted agents to be able to mine on the network. This behaviour is closer
to a shared database than a secure ledger. Nevertheless, we discussed this in
\cref{sec:combiningThemLR} and this is the desired behaviour for a local
blockchain used inside a cluster of robots.

Another result is the number of empty blocks we get, because miners are mining
faster than the rate of transactions being submitted by the network.  A
solution to prevent this from happening is to set up an event listener which
only activates the miners when there are pending transactions available,
following these instructions \autocite{onlyMineWhenPendingTransactions}. Note
that this has not been tested for this project.

\paragraph{Interval of Time between two Auctions}

As robots are only able to participate in an auction if they are in an
\verb+idle+ state, this is the parameter with the strongest effect on the
achievement of a set of tasks since we set the mining complexity to zero.

Tests have been conducted with the execution of a script, automatically
triggering a sequence of auctions of tasks stored in an array (see
\cref{table:testingLocations}). These auctions were launched at a regular
interval and used three different duration intervals: $2$; $10$ and $20$
seconds in a team of $5$ robots with a mining complexity of $0$. Between the
tests, the smart-contract was redeployed to have the same starting conditions.
The results are shown in \cref{table:resTimeBetweenAuctions}.

\begin{table}[!h]
    \centering
    \begin{tabular}{ |c|c|c|c|c|c|c| }
        \hline
        Task Id & Reward (eth) & $x$ & $y$ & $z$ & Orientation (z) & waitTime \\
        \hline
        0& 150&  0&  0& 0& 0& 10\\
        1& 150&  3&0.5& 0& 0& 20\\
        2& 150&  3& -3& 0& 0& 30\\
        3& 150& -3&  2& 0& 0& 40\\
        4& 150& -4& -4& 0& 0& 30\\
        5& 150&  1& -5& 0& 0& 20\\
        6& 150&  4&  4& 0& 0& 10\\
        7& 150& -1& -1& 0& 0& 20\\
        8& 150&  2&  3& 0& 0& 30\\
        9& 150& -5&  1& 0& 0& 10\\
        \hline
    \end{tabular}
    \caption{List of Testing Locations}
    \label{table:testingLocations}
\end{table}

\begin{table}[!h]
    \centering
    \begin{tabular}{ |c|c|c| }
        \hline
            $2$ sec & $10$ sec & $20$ sec\\
        \hline
            $3$ done            & $7$ done            & $8$ done\\
            $1$ discarded       & $2$ discarded       & $2$ discarded\\
            $6$ not transferred & $1$ not transferred & $0$ not transferred\\
        \hline
    \end{tabular}
    \caption{Number of Tasks Done out of $10$, after $5$ minutes, based on the Interval of
        Time between Two Auctions Opened by the Server}
    \label{table:resTimeBetweenAuctions}
\end{table}

In the first experiment, $6$ tasks have not been auctioned because they were
sent too fast to the \verb+traderNode+ as an auction lasts for at least $5$
seconds. This only happened once in the second experiment, but for another
reason: there was no robot available for bidding as they were all doing
something (this happened when tasks with $20$; $30$ and $40$ seconds of
duration were being performed). Finally, this did not happen in the last
experiment as a robot was available to perform the task that was lost earlier.
Out of the $10$ tasks, there were $2$ that were not doable as the goal location
was either outside the map used by the Navigation Stack or occupied by an
obstacle.

\paragraph{Number of Robots}

The system can easily accommodate teams of different sizes. The only
requirement before adding a new robot is to create a new wallet on one of the
blockchain nodes.  In the auction of a task, this parameter does not influence
the system in terms of delay overheads because the duration of an auction has
been set to $5$ seconds.  This is necessary because we cannot know how many
robots will bid on the task, and some robots may take a longer time to estimate
their bid, especially if they have recursive auctions to run (see
\cref{sec:longTerm}).  With an average time of $5.5$ seconds to run an auction,
the quality of the network has more influence than the number of robots.

However, this parameter influences the outcome of a set of tasks as there are
more robots available.  A result that appeared during this testing is, as the
locations to go to were distributed on the map (globally in each corner and in
the center), with $5$ robots, the system could reach a stable state in which
robots were distributed on the map, minimizing hence the distance they had to
do to perform the different tasks.

\paragraph{Length of the Sequence of Tasks}

This only influences the total duration of a full mission composed of a
sequence of tasks. The longest test made in order to stress the system showed
that it could at least support $63$ of those in a team of $5$ robots, without
refreshing the {\DH}App or restarting the \gls{ros} nodes. This result was
obtained by triggering the testing script several times (interval time of $10$
seconds and no mining complexity), and by manually adding some tasks during its
execution. Because the {\DH}App is based on JavaScript events, minting the
token did not create any issue. However, because only one auction can be
triggered at a time in the \gls{mrs} either the manually input task or one of
the script was lost, because discarded by the \verb+traderNode+ running on the
server. This depends on which callback function was called first.

The system can handle a large number of deployed tasks. However, there are
some weaknesses that downsize this: even though the system did not have to be
restarted, some errors started to appear in the simulation. Gazebo and Rviz
could show the same robot at two different locations, which troubled the
execution of some tasks because the Navigation Stack of affected robots
received conflicting information.

Running a long simulation with $5$ robots seemed to be less stable than with
only $3$ robots, as either map misalignment issues or a robot seeming stuck for
no reasons. To further investigate this, mining on another computer would be
suitable as the processing power of workstation would completely be allocated
to the simulation.


% \subsubsection{Security Testings}

% Due to a lack of knowledge, security of the system has not been tested. We will
% discuss this more in \nameref{sec:systemAssessment}
% (\cref{sec:systemAssessment}) and \nameref{sec:futureWork}
% (\cref{sec:futureWork}).

\subsubsection{Usability Testings}
\paragraph{{\DH}App - Web Interface}

The interface is composed of three sections as seen in \Cref{fig:webInterface}.
On the right, the operator inputs information on a new mission; in the middle
are displayed information on the different wallets, such as their ether and
token balances, as well as information on tokens that are available on the
blockchain such as id, name, status, owner, and information. Lastly, on the
right, logs on interactions with the \gls{ros} system and the blockchain node
are displayed. These can easily be hidden.

\begin{figure}[!h]
    \centering
    \vspace*{0.5cm}
    \advance\leftskip-2.3cm
    \includegraphics[width=19cm]{system_testing/dappInterface.png}
    \caption[{\DH}App - Web Interface - of the System]
            {{\DH}App - Web Interface - of the System where the Operator can
            input new Missions, check the Status of the Different Tokens and
            Read the Interactions with the Blockchain node.}
    \label{fig:webInterface}
\end{figure}

\paragraph{Simulation Tools}

Rviz and Gazebo are used to run simulations and they both have clear interfaces
to follow robots movements.

% ############################################################################ %
\subsection{Testings Summary}

\Cref{table:summaryRequirements} summarizes the requirements that have been met.
Most of them have been and those which have not been are discussed in
\cref{sec:systemAssessment} and \cref{sec:futureWork}.

\begin{table}[!h]
	\centering
    \advance\leftskip-1.6cm
    \begin{tabular}{ |c|c|c| }
        \hline
        Requirements & Status & Testing Method \\
        \hline

        Incomplete Information on the Environment
        & In \verb+Navigation Stack+
        & Partially in Simulation
        \tablefootnote{Further tests with a physical deployment are needed}
        \\

        Deal with Imperfect Communication
        & \verb+Unknown+
        & Not tested
        \\

        Handle new Instructions During a Mission
        & \verb+Implemented+
        & Simulation
        \tablefootnote{This could be improved by letting robots participate in
            auctions while they are busy}
        \\

        Accommodate a New Robot
        & \verb+Done+
        & Simulation
        \tablefootnote{If it is a completely new robot, a configuration process
            is required, such as making sure it can use a wallet on one of
            the blockchain nodes; or if it runs its own, adding it to the
            network if it can be trusted.}
        \\

        Flexibility in Task Execution
        & Simplified
        & Simulation
        \\

        Resistance to "Malicious" Robots
        & \verb+Unknown+
        & Not tested
        \\

        Recursive Auctions
        & \verb+Not Implemented+
        & {\o}
        \\

        Robustness to Failure
        & \verb+Partially+
        & See \cref{sec:robustnessEvaluation}
        \\

        Modular and Flexible Design
        & \verb+Done+
        &
        \\

        \hline
    \end{tabular}
    \caption{Summary of the System Requirements}
    \label{table:summaryRequirements}
\end{table}

