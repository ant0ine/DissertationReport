% !TEX root:Report.tex
\newacronym{mrs}{MRS}{Multirobot System}
\newacronym{AIRA}{AIRA}{Autonomous Intelligent Robot Agent}

\graphicspath{{./DissertationPortfolio/LatexReport/Chapters/2_LiteratureReview/resources/},{./Chapters/common_resources/}}

\chapter{Problem Definition}
\label{sec:problemDefinition}

\section{Description of the Project}
\label{sec:projectDescription}

\glsreset{mrs}
\subsubsection{\gls{mrs}}
As the urge for automata being able to perform more and more complex tasks
increases, the focus moves on to the development of teams of robots. By dividing
a single task into sub-tasks, a group of robots can execute them more quickly,
improving the solution quality and the robustness of the system. The key
point resides, hence, in having a coordination algorithm able to divide tasks
and allocate them to the robots.

While using a fully centralized system has its advantages (\textit{a central
    computer generates and assigns tasks to every robot of the team}), this
project will focus on a more local and distributed approach, in which the
robots decide themselves what to do. This approach requires far less processing
power and is more robust to changing environments.

These robots can communicate with each other, and more importantly
\textbf{trade}, leading to a rising research field: \textit{an open-market
based multirobot system}. Robots act in their self-best interest, looking to 
maximize their own profit by reducing their costs. Hence, they trade time, tasks
and resources. This self-interest behavior leads to the maximization of the 
overall profit.


\subsubsection{Blockchain and Smart-contracts}
% In 2017 happened the explosion of the exchange rate of bitcoin and other 
% crypto-currencies, reaching \$18.000 in November [ref needed].

A blockchain can be seen as an open and public ledger which every member of a
network stores and shares. This ledger is a database of transactions between
agents. Transactions are grouped into "blocks" which contain the identifier of
the previous block, the information about a number of transactions and an
answer to a cryptographic problem which makes the block valid. Once a block is
added to a blockchain, it is impossible to modify. This technology shows that a
distributed group of agents can reach an agreement without any central and
trusted intermediary.

A smart-contract can be seen as an autonomous script stored on the blockchain.
(Note that, not all blockchains support smart-contracts). This script
describes a complex set of activities which will be executed once a transaction
will be addressed to it. As a smart-contract deployment is stored in a block,
the script itself is immutable. This means, that once deployed, a contract
cannot be modified, and if well-written, is safe and secure to use.

\subsubsection{Combining Them}

The novelty of this project resides in the merging of these two uprising
fields. The literature on \gls{mrs} makes the assumption that robots are
trustworthy and reliable. However, in the context of a democratization of such
systems, security, trust, accountability, and privacy become dealbreakers. This
is where the key advantages of blockchain are interesting: different robots,
from different manufacturers, do not necessarily trust each other. Yet, they
could reach a consensus and work together by trusting the cryptographic
algorithm more than their teammates.

In the context of the open-market economy, a smart-contract could be used to
describe a task trade between two robots, ensuring that a robot is getting paid
for its services and that the transaction really happened. As the added-value
of using a blockchain relies on a distributed and incorruptible ledger of
transactions, this could make existing coordination algorithms robust to
malicious applications. Moreover, a blockchain can also provide robustness to
data storage and secure logging. An unusual behaviour would be more easily
detected because it will be easier to prove that a robot misbehaved. An
appropriate action, such as the exclusion of the system, shall be taken then. 


\begin{figure}[!h]
    \centering
    \includegraphics[width=12cm]{Capella/CSA_System.png}
    \caption{CSA - Contextual System Actors}
    \label{fig:csa}
\end{figure}

\Cref{fig:csa} shows the different actors involved in the system designed in
this project. 

\section{Aims and Objectives}
\label{sec:aimsObjectives}

\subsection{Aims}
This project aims to study the added-value of using the blockchain in a
\gls{mrs} by developing an open-market economy where agents can safely trade 
with each other. The blockchain based architecture is aimed at solving issues
of accountability, trust, privacy, and reliability in a \gls{mrs}.

\Cref{fig:ocb} shows the operational capabilities of the system.

\begin{figure}[!h]
    \centering
    \includegraphics[width=\textwidth]{Capella/OCB_Operational_Capabilities.png}
    \caption{OCB - Operational Capabilities Breakdown Diagram}
    \label{fig:ocb}
\end{figure}

\subsection{Objectives}
The objectives to fulfill this aim fall into different categories and are
presented as the followings:

\begin{itemize}[noitemsep]
    \item Preparation:
        \begin{itemize}[noitemsep]
            \item Gain a global view of methods and technologies underpinning an
                open-market economy for \gls{mrs} by studying the literature
            \item Look for work that has used blockchain technology
                to address problems of liability, security and trust in 
                related fields
            \item Investigate design options and issues for the implementation
                of an open-market economy based on the results of the above
                researches
            \item Investigate design options and issues for the implementation 
                of a blockchain, smart contracts and tokens
            \item Identify key advantages of implementing a blockchain for
                multirobot systems
        \end{itemize}
    \item Development:
        \begin{itemize}[noitemsep]
            \item Designing a basic scenario including several robots
                interacting with each other
            \item Implementing a basic open-market economy for robots
            \item Implementing smart-contracts in this open-market economy
            \item Design different software architectures, more or less
                centralized
        \end{itemize}
	\item Testing:
         \begin{itemize}[noitemsep]
            \item Run the scenario on simulation and with real robots
            \item Run the scenario on different software architectures to
                better extract the effects of the blockchain on the system
            \item Identify drawbacks and improvements
        \end{itemize}   
    \item Analyzing the results:
        \begin{itemize}[noitemsep]
            \item Evaluate the reliability of the implementation and the added
                benefits
            \item Evaluate the effectiveness of the implementation and the
                induced overhead of using blockchain in comparison to the 
                different software architectures
        \end{itemize}
\end{itemize}

%% Sections on Background / Technical Review
%% -----------------------------------------
\section*{Background}
%% The path are given from the root file
The following sections are a presentation of the current literature on the subject.
\input{DissertationPortfolio/LatexReport/Chapters/2_LiteratureReview/1_MultiRobotSystem.tex}
\input{DissertationPortfolio/LatexReport/Chapters/2_LiteratureReview/2_Blockchain.tex}
\input{DissertationPortfolio/LatexReport/Chapters/2_LiteratureReview/3_CombiningThem.tex}

%% Short section at the end of the LR to include AIRA Life Project
\subsection{An Actual Implementation of a Blockchain in Robotic System}

\textcite{BlockchainFrameworkForRoboticSwarmSystems} is the first to introduce
the idea of using a blockchain in a swarm robotic system. This paper is
conceptual; however, in July 2018, \textcite{swarmRobotsBlockchainDorigo}
presented a primarily work on implementing a blockchain in a swarm robotic
system. By doing so, they bring trust to the system which is able to detect
Byzantine robots and to exclude them from the swarm. They had different
objectives than the present project and focus on swarm robots. For these
reasons in addition of the late publication, their work could not have been
used in this project.

\gls{AIRA} Project \autocite{airaProjectMainPage} aims at developing a
"standard of economic interaction between human-robot and robot-robot via
liability smart contract". Their solution is based on the Ethereum blockchain
and focus on large-scale interaction such as smart-cities. This looks promising
as they make a large use of the blockchain to develop a secure and safe
solution to reach an agreement. They are developing a transaction protocol
combining the economic and technical aspects of communication into one
transaction.

However, they do not seem to cluster devices (they refer to these as
"Cyber-Physical Systems", in order to improve the scalability of their
solution. In addition, \gls{AIRA} is fully based on the Ethereum main
network, and hence, is prone to latency transaction when triggering a
transaction.

\begin{figure}[!h]
    \centering
    \includegraphics[width=\textwidth]{aira_project.jpg}
    \caption{AIRA aims at interfacing human-robot and robot-robot interactions. 
        Taken from \autocite{airaProjectMainPage}.}
    \label{fig:airaProject}
\end{figure}

Even though the project is open-source \autocite{airaGithub}, it is not a
suitable basis for this dissertation because of the little documentation of the
code, their objectives and the features they implemented (half of what is
documented is in Russian). Moreover, they have not published any scientific
paper on their work yet. They approached the problem from the blockchain point
of view, whereas we are approaching it from the open-market economy for robots
side with the objective of improving multirobot systems.

% \clearpage
\section{Design Requirements}
\label{sec:designRequirements}

\subsection{Features}

From the literature discussed in
\cref{sec:mrsLR,sec:blockchainLR,sec:combiningThemLR}, we extracted important
features. The followings are the requirements for the system:

%TODO: write more as intro
\begin{itemize}
    \item Ability to execute a task with incomplete information about the
        environment. Because the environment is dynamically changing (whether it
        is by the robot themselves or not), the task planner must be able to
        quickly self-adapt to the new situation.
    \item Ability to deal with imperfect communication: to make sure that
        agents can communicate even if they experience some jitters.
    \item Dynamically handle new instructions from the operator during a 
        mission: the system must be able to adjust its original plans according
        to updated information.
    \item Ability to accommodate a new robot at any moment during a mission: the
        size of the team is dynamic to ensure the best possible response
        to performing a common task.
    \item Flexibility to execute different tasks: to bring flexibility and
        purpose in having a \gls{mrs}.
    \item Resistance to "malicious" robots: someone could try to tamper with a
        mission or its results. The system must have a secure storage to
        ensure data are genuine.
    \item Recursive auction: if a robot needs to hire another one or if it is
        bound to others for a mission, it will ask a price to compensate a
        breach of contract.
    \item Robustness to failures: an autonomous system must be able to cope
        with problems without any human intervention. See
        \cref{sec:robustnessFailure} for more information.
    \item A modular and flexible design of the implementation of the open-market
        in order to bring interoperability and make improvements easier to
        implement.
\end{itemize}

\subsection{Robustness to Failures}
\label{sec:robustnessFailure}

In autonomous systems such as robots, being able to deal with failures is
extremely important because the system is on its own and cannot require a human
to take a decision. The following are a set of failures our system should be
able to deal with.

\begin{itemize}
    \item \textbf{Unplanned obstacle on a path:}
        the original plan cannot be followed. Sensors must be used to detect
        the size of the obstacle in order to compute another trajectory to
        reach the point. If it is still not possible, the task should be
        re-auctioned so that, another robot could try to reach the goal from a
        different starting point.
    \item \textbf{Failed robot still in communication:}
        but does not know it is faulty (exclusion by supervisor) or knows it is
        faulty (self-exclusion) and hence re-auction its allocated tasks.
    \item \textbf{Failure than recovery:}
        a robot is available again. An example is a robot flagging itself as
        unavailable because it must recharge its battery. Once they are charged
        enough, the robot is available again.
    \item \textbf{Crash of a robot (\textit{robot death}):}
        a robot stops being operative. In this, case, the whole system
        suddenly shuts down.
\end{itemize}

As mentioned earlier, this non-exhaustive list of failures indicates the level
of robustness of the system.

\subsection{Taxonomies}
\label{sec:taxonomies}

A taxonomy is a classification into ordered groups or categories
\autocite{americanHeritageDic}. The main point of these is to cover the all set
of actions a robot or its team can achieve. If a robot does not understand an
element of one of these taxonomies, it could simply reject it, making this
assumption that, if it does not know, it cannot do it.  To be the most
flexible, the system requires to have a set of taxonomies in these different
categories:

% metrics, tasks, "interactions" (call for help, sell tasks), available resources
\begin{itemize}
    \item \textbf{Tasks:}
        each doable tasks must be decomposed in a simpler set of actions
        to let robots understand them more easily.
    \item \textbf{Metrics:}
        the metrics functions are used to estimate the cost of
        performing a task before deciding to bid or not on it.
    \item \textbf{Interactions:}
        some tasks could require tight coordination between two
        or more robots (such as the "box pushing" mission presented in
        \autocite{AuctionMethodsForMultirobotCoordination}). Having a taxonomy
        of interaction enables a mission to be more complex.
    \item \textbf{Available resources:}
        a taxonomy about resources let the robots decide
        if they are physically able to perform a task. For instance, a task
        could consist in monitoring a specific area and track a moving element.
        Performing this mission would require the use of a camera or a Lidar.
\end{itemize}

Moreover, having defined taxonomies brings flexibility and interoperability
to the system. Indeed, if another system were to use the same trading protocol,
but with different internal components, it would be able to interact with
the design of this project. This is, however, beyond the scope of the project.

\section{Outline}
% remainder of the structure of the dissertation

The rest of this paper is organized as follows: \cref{sec:designDescription}
reviews the methodology and the design of the system, starting with an overview
of the system and a use case scenario. It is followed by a more detailed
description of the designed architecture and ends with a description of its use
by the final user. \cref{sec:evaluation} evaluates the system towards the
requirements discussed in \cref{sec:designRequirements}, then assess the system
and discusses ideas to improve the design and the implementation of the project
on different time scales. \cref{sec:mainConclusion} concludes this paper.
