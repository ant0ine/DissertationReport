\documentclass[12pt,a4paper,twoside,openright]{report} 
% \usepackage[utf8]{inputenc}
\usepackage{booktabs}
\usepackage{bookmark}
% \usepackage{caption}
\usepackage{csquotes} % Recommended

\usepackage{filecontents}
\usepackage[style=ieee,citestyle=ieee,backend=biber]{biblatex}
\addbibresource{./Chapters/report.bib}
\addbibresource{./DissertationPortfolio/LatexReport/Chapters/notes.bib}

%% Graphics
\usepackage{graphicx} % For graphics (ie images)
\usepackage{rotating}

\usepackage[export]{adjustbox}
\usepackage[english]{babel}

%% Configure the document
\usepackage[a4paper,bindingoffset=0in,
            left=1.24in,right=1.24in,top=1in,bottom=1in,
            footskip=0.5in]{geometry}
\setlength\parindent{0pt}
\setlength{\parskip}{10pt plus 1pt minus 1pt}

\usepackage{emptypage}
\usepackage{listings}
\usepackage{color}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{enumitem}
% \usepackage{array}
\usepackage{mathtools}

%% Table packages
\usepackage{multicol}
\usepackage{multirow}
\usepackage{tablefootnote}

%% Citation
%\usepackage[hypertexnames=false]{hyperref}
\usepackage{hyperref}
\usepackage[capitalise]{cleveref}

%% Appendices
\usepackage[titletoc,title]{appendix}

%% ACRONYMS
%% --------
%% Load the package with the acronym option & generate the glossary
\usepackage[toc,acronym,nomain]{glossaries}
\makeglossaries
%% How to use:
%% \newacronym{pow}{PoW}{Proof of Work}

%% NOMENCLATURE 
%% ------------
%% Load package for Nomenclature and generate it
\usepackage[intoc,english]{nomencl}
\makenomenclature
%% How to use:
%% \nomenclature{$c$}{Speed of light in a vacuum inertial frame}

%% To use Sans Serif font
% \renewcommand{\familydefault}{\sfdefault}

\title{Dissertation Portfolio}
\author{Antoine Cougny}
\date{March 2018}

% \usepackage[mastersc]{edmaths}

\begin{document}

\pagenumbering{Roman}

\begin{titlepage}
    \begin{figure}[htp]
        \includegraphics[scale=1, right]{Introduction/heriot-watt_logo.png}
        \label{logo1}
    \end{figure}

    \begin{center}
        \vspace{0cm}
        {\scshape\LARGE Heriot-Watt University \par}
        \vspace{0.6cm}
        {\scshape\LARGE School of Engineering and Physical Sciences \par}
        \vspace{2cm}
        {\scshape\Large MSc in: Robotics, Autonomous and Interactive
                Systems \par}
        \vspace{0.6cm}
    {\scshape\Large Dissertation \par}
	\end{center}
	\vspace{2cm}
    {\scshape\Large Title: An Open-Market Economy for Multirobots Systems
            Secured By Blockchain\par}
	\vspace{0.6cm}
    {\scshape\Large Author: Antoine COUGNY\par}
	\vspace{0.6cm}
	{\scshape\Large Matriculation Number: H00280347 \par}
	\vspace{0.6cm}
    {\scshape\Large Date: August 2018\par}
	\vspace{0.6cm}
    {\scshape\Large Supervisor: Dr.~Mauro Dragone\par}
    \vfill
\end{titlepage}

% \pagenumbering{roman} % page numbers in roman numbers

%% Declaration Authorship
\input{Introduction/blankPage.tex}
\input{Introduction/declarationAuthorship.tex}

%% Abstract
\input{Introduction/blankPage.tex}
\input{Introduction/abstract.tex}

%% Dedication
\input{Introduction/blankPage.tex}
\input{Introduction/dedication.tex}

%% Acknowledgements
\input{Introduction/blankPage.tex}
\input{Introduction/acknowledgement.tex}
% \input{Introduction/blankPage.tex}


% \setcounter{secnumdepth}{3}
\setcounter{tocdepth}{1}
\tableofcontents
\begingroup
    %% Glossaries
    \printglossaries
    % \let\clearpage\relax

    %% Nomenclature
    % \printnomenclature

    %% Figures
    % \addcontentsline{toc}{chapter}{List of Figures}
    \listoffigures
    % \let\clearpage\relax

    %% Tables
    % \addcontentsline{toc}{chapter}{List of Tables}
    \listoftables
    \let\clearpage\relax

    %% Listings
    \lstlistoflistings
\endgroup


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Start of the main part 
\cleardoublepage
\setcounter{page}{1}   % reset page counter
\pagenumbering{arabic} % page numbers in arabic numbers

\input{Chapters/1_Intro/introduction.tex}
\input{Chapters/2_DesignDescription/0_DesignDescription.tex}
\input{Chapters/3_Evaluation/0_evaluation.tex}
\input{Chapters/4_Conclusion/conclusion.tex}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Appendices
\begin{appendices}
    \chapter{Additional Information on Blockchain}
    \input{./Appendices/hyperledger_fabric.tex}
    \input{./Appendices/ipfs.tex}

    \chapter{Additional Information on the System Architecture}
    \input{./Appendices/systemAnalysis.tex}
    \input{./Appendices/logicalArchitecture.tex}

    \chapter{Additional Logs}
    \input{./Appendices/logsTaskDeployment.tex}
\end{appendices}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newpage
\printbibliography


\end{document}

